Ansible DNSmasq
===============

Ansible role to install and configure dnsmasq on a Debian flavored Linux.

Requirements
------------

Tested with:

- Raspberian 9
- Ansible 2.7

Role Variables
--------------

See example.

Dependencies
------------

None.

Example Playbook
----------------

*Inventory:*

```plain
[nameserver]
ns1 ansible_host=192.168.1.2 ansible_user=pi

[nameserver:vars]
domain=example.com
```

*Playbook:*

```plain
---
- hosts:
    - nameserver

  roles:
    - role: ansible-dnsmasq
      vars:
        dnsmasq_overlay_1:  |
          domain={{ domain }}
          local=/{{ domain }}/
          addn-hosts=/etc/hosts.dnsmasq
          resolv-file=/etc/resolv.conf.upstream

          dhcp-range=192.168.1.200,192.168.1.250,12h
          dhcp-option=option:router,192.168.1.1
          dhcp-option=option:dns-server,192.168.1.2
          dhcp-option=option:netmask,255.255.255.0

          dhcp-host=00:50:56:01:23:45,192.168.1.201

        hosts_dnsmasq_1: |

          192.168.1.9 somehost.{{ domain }} somehost
          192.168.1.211 somehost2.{{ domain }} somehost2
```

License
-------

BSD

Author Information
------------------

fwalk___gitlab
